use legalzinc::*;
use std::fs;
use std::io::prelude::*;
fn main() {
    let mut map = WordMap::new();

    let menu = "1) Parse book directory\n2) Markov chain\n3) Weighted Markov chain";

    println!("Legal Zinc testing tool");
    loop {
        println!("{}", menu);
        print!("Enter a menu option: ");
        std::io::stdout().flush().unwrap();

        let mut input = String::new();
        std::io::stdin().read_line(&mut input).unwrap();
        let input = match input.trim().parse::<u32>() {
            Ok(val) => val,
            Err(_) => {
                println!("Invalid input. Try again.");
                continue;
            }
        };
        match input {
            1 => {
                let paths = fs::read_dir("books/").unwrap();
                for (index, path) in paths.enumerate() {
                    println!("{}", index);
                    let path = path.unwrap().path();
                    let contents = fs::read_to_string(path).unwrap();
                    map.parse(contents.as_str(), 3);
                }
            }
         
            2 => {
                println!("\n{}\n", map.markov_chain(10, 3));
            }
            3 => {
                println!("\n{}\n", map.weighted_markov_chain(10, 3));
            }
            _ => {
                println!("Invalid choice. Try again.");
                continue;
            }
        }
    }
}
